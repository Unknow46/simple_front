FROM node:16-alpine3.11
WORKDIR /app
COPY . /app

RUN npm i
EXPOSE 8000
CMD ["npm", "start"]

